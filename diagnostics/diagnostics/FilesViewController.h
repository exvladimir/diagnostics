//
//  FilesViewController.h
//  diagnostics
//
//  Created by Рашкин Владимир Валерьевич on 25/05/16.
//  Copyright © 2016 vrashkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "FilesAll.h"

#define ApplicationDelegate                 ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface FilesViewController : UITableViewController <NSFetchedResultsControllerDelegate>{
    
}

@property(nonatomic, retain) IBOutlet UITableView * o_FilesTable;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@end
