//
//  AppDelegate.m
//  diagnostics
//
//  Created by vrashkin on 22/02/15.
//  Copyright (c) 2015 vrashkin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - UIApplicationDelegate

- (instancetype)init{NSLog(@"- (instancetype)init");
    
    AppDelegate *tecSelf = [super init];
    
    [tecSelf initBeginCD];
    
    return tecSelf;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {allLogCollector(@"->",vivodProcedur,true,true);
    
    //Запускаем в бекграунде
    self.backgroundTaskIdentifier =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
        [self runAfter_applicationDidFinishLaunching];
        
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
    }];
    
    // Override point for customization after application launch.
    
//    //Регимся на нотификацию
//    UIUserNotificationType userNotificationTypes = UIUserNotificationTypeBadge;
//    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];
//    [application registerUserNotificationSettings:settings];
//    //И только тогда отправляем уведомления
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:010];
//    
//    [self performSelectorInBackground:@selector(runAfter_applicationDidFinishLaunching) withObject:nil];
    
    tabBarController = (UITabBarController *)self.window.rootViewController;
    navigationController = [tabBarController viewControllers][0];
    filesViewController = [navigationController viewControllers][0];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {allLogCollector(@"->",vivodProcedur,true,true);
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{allLogCollector(@"->",vivodProcedur,true,true);
    
    [self saveContext:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {allLogCollector(@"->",vivodProcedur,true,true);
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {allLogCollector(@"->",vivodProcedur,true,true);
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {allLogCollector(@"->",vivodProcedur,true,true);
    [self saveContext:YES];
}

#pragma mark - Селекторы
- (void)runAfter_applicationDidFinishLaunching{allLogCollector(@"->",vivodProcedur,true,true);
    
    //Запускаем опрос хоста и порта на доступность. Яндекс (ya.ru:80), Гугл(google.com:80), Аппл.
    
}

#pragma mark - outputLogCollector
- (void) sendLogTo{
    //NSOutputStream
    if (outputLogCollector == nil) {
        
        //Выбираем сообщения из логов
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSSortDescriptor *timedateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timedate" ascending:NO];//ascending:YES];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:timedateDescriptor, nil];
        assert(timedateDescriptor != nil);
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSManagedObjectContext *context = [self managedObjectContext];NSError *error = nil;
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"LogDevice" inManagedObjectContext:context];
        assert(entity != nil);
        [fetchRequest setEntity:entity];
        
        NSArray *resultsZaprosAll = [context executeFetchRequest:fetchRequest error:&error];
        //        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"LogDevice"];
        //        NSArray *resultsZaprosAll = [context executeFetchRequest:request error:&error];
        
        if (([resultsZaprosAll count]>0)||([cmdLG count]>0)) {
            
            NSLog(@"sendLogTo'%lu' [cmdLG count]'%lu'",(unsigned long)[resultsZaprosAll count],[cmdLG count]);
            
            firstMess=YES;
            
            CFReadStreamRef readStream;
            CFWriteStreamRef writeStream;
            //CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"afaria.innoway.ru", 3009, &readStream, &writeStream);
            CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"192.168.88.179", 3009, &readStream, &writeStream);
            
            inputLogCollector = (__bridge_transfer NSInputStream *)readStream;
            outputLogCollector = (__bridge_transfer NSOutputStream *)writeStream;
            [inputLogCollector setDelegate:self];
            [outputLogCollector setDelegate:self];
            [inputLogCollector scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [outputLogCollector scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [inputLogCollector open];
            [outputLogCollector open];
        }
    } else {
        //Уххх не помню
        //TODO Решить что будет если вызвали функцию с открытым потоком
    }
    
}

- (void) initBeginCD{NSLog(@"initBeginCD");
    //Инициализация контекстов CoreData
    initMOC=NO;
    messagesLG=[NSMutableArray new];
    cmdLG=[NSMutableArray new];
    not_send_messagesLG=YES;
    
    //dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        _daddyManagedObjectContext = [[NSManagedObjectContext alloc]  initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        
        //dispatch_sync(dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            _managedObjectContext = [[NSManagedObjectContext alloc]  initWithConcurrencyType:NSMainQueueConcurrencyType];
            // Добавляем наш приватный контекст отцом, чтобы дочка смогла пушить все изменения
            [_managedObjectContext setParentContext:_daddyManagedObjectContext];
            assert(_managedObjectContext!=nil);
            [self initEndLC];
        });
        
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        [_daddyManagedObjectContext setPersistentStoreCoordinator:coordinator];
        
    });
    
}

- (void) initEndLC{NSLog(@"initEndLC");
    initMOC = YES;
    allLogCollector(@"->",vivodProcedur,true,true);
    NSLog(@"[self saveContext:NO];");
    [self saveContext:NO];
    
    //Обновим все таблицы связанные с CoreData
    [filesViewController.o_FilesTable reloadData];
    
    //Проработаем получение всех файлов при первой загрузки
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults stringForKey:@"getAllFile"]==nil) {
        [cmdLG addObject:@"getAllFile"];
    }
    
    //Посылаем логи
    [self sendLogTo];
    //Взводим таймер посыла логов
    NSTimeInterval vibTimeSynchronize = 5;//sec
    self.syncTimerLG = [NSTimer scheduledTimerWithTimeInterval:vibTimeSynchronize target:self selector:@selector(sendLogTo) userInfo:nil repeats:YES];
    
}

- (void) cleanCData:(NSArray *)tekElements{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for (NSManagedObject* tmpObject in tekElements) {
        
        [context deleteObject:tmpObject];
    }
    NSError *error = nil;
    if (![context save:&error]) {
        NSString *tstStr=[[NSString alloc] initWithFormat:@"ОШИБКА при сохранении удаленных %@, %@", error, [error userInfo]];
        allLogCollectorNSLog(tstStr, 5, true,true,true);
        //abort();//Выйти из программы и
    }
}

- (NSManagedObjectContext *)getContextForBGTask {//NSLog(@"getContextForBGTask");
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    assert(_managedObjectContext!=nil);
    [context setParentContext:_managedObjectContext];
    return context;
}

- (void) SohraniatMessInCDWithOrder:(NSString*)sendingMessage appearance:(NSInteger)tekAppearance modul:(NSString*)tekModul procedure:(NSString*)tekProcedure numberLine:(NSInteger)tekNumberLine
                         code_order:(NSString*)tekCode_order action_order:(NSString*)tekAction_order counteragent_order:(NSString*)tekCounteragent_order guid_order:(NSString*)tekGuid_order{
    //Если инициализирован CD
    if (initMOC) {
        //Записываем в бекграунде
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSManagedObjectContext *context = [self managedObjectContext];
            assert(context != nil);
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"LogDevice" inManagedObjectContext:context];
            assert(entity != nil);
            
            //        NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:_managedObjectContext];
            NSManagedObject *object = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
            
            NSString *activeObject = [NSString stringWithFormat:@"(%@)(%@)(%@)(%@)",tekAction_order,tekCode_order,tekCounteragent_order,tekGuid_order];
            
            [object setValue:activeObject forKey:@"activeObject"];//string
            [object setValue:[[NSNumber alloc] initWithInteger:tekAppearance] forKey:@"appearance"];//int
            [object setValue:sendingMessage forKey:@"body"];//string
            [object setValue:tekProcedure forKey:@"comment"];//string
            [object setValue:tekModul forKey:@"modul"];//string
            [object setValue:[[NSNumber alloc] initWithInteger:tekNumberLine] forKey:@"numberline"];//int
            [object setValue:[[NSNumber alloc] initWithInteger:numberzapusk] forKey:@"numberzapusk"];//int
            [object setValue:[NSDate new] forKey:@"timedate"];//NSDate
            
        });
    } else {
        //TODO Копим логи в памяти
        
    }
    
}

- (void) SohraniatMessInCD:(NSString*)sendingMessage appearance:(NSInteger)tekAppearance modul:(NSString*)tekModul procedure:(NSString*)tekProcedure numberLine:(NSInteger)tekNumberLine{
    //Если инициализирован CD
    if (initMOC) {
        //Записываем в бекграунде
        //        NSLog(@"Записываем в бекграунде");
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSManagedObjectContext *context = [self getContextForBGTask];
            assert(context != nil);
            LogDevice *object = [NSEntityDescription insertNewObjectForEntityForName:@"LogDevice" inManagedObjectContext:context];
            [object setValue:@"DFS" forKey:@"activeObject"];//string
            [object setValue:[[NSNumber alloc] initWithInteger:tekAppearance] forKey:@"appearance"];//int
            [object setValue:sendingMessage forKey:@"body"];//string
            [object setValue:tekProcedure forKey:@"comment"];//string
            [object setValue:tekModul forKey:@"modul"];//string
            [object setValue:[[NSNumber alloc] initWithInteger:tekNumberLine] forKey:@"numberline"];//int
            [object setValue:[[NSNumber alloc] initWithInteger:numberzapusk] forKey:@"numberzapusk"];//int
            [object setValue:[NSDate new] forKey:@"timedate"];//NSDate
            
            if ([context hasChanges]){
                NSError *error = nil;
                if (![context save:&error]) {
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }
            }
        });
    } else {
        //TODO Копим логи в памяти
        LogDevice_NotSynch *tekObjectLog = [LogDevice_NotSynch new];
        tekObjectLog.activeObject =@"DFS";
        tekObjectLog.appearance = [[NSNumber alloc] initWithInteger:tekAppearance];
        tekObjectLog.body =sendingMessage;
        tekObjectLog.comment =tekProcedure;
        tekObjectLog.modul =tekModul;
        tekObjectLog.numberline =[[NSNumber alloc] initWithInteger:tekNumberLine];
        tekObjectLog.numberzapusk =[[NSNumber alloc] initWithInteger:numberzapusk];
        tekObjectLog.timedate =[NSDate new];
        [messagesLG addObject:tekObjectLog];
    }
    
}

- (NSString *)getIPAddress_en0 {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}

- (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_VPN_LC @"/" IP_ADDR_IPv4_LC, IOS_VPN_LC @"/" IP_ADDR_IPv6_LC, IOS_WIFI_LC @"/" IP_ADDR_IPv4_LC, IOS_WIFI_LC @"/" IP_ADDR_IPv6_LC, IOS_CELLULAR_LC @"/" IP_ADDR_IPv4_LC, IOS_CELLULAR_LC @"/" IP_ADDR_IPv6_LC ] :
    @[ IOS_VPN_LC @"/" IP_ADDR_IPv6_LC, IOS_VPN_LC @"/" IP_ADDR_IPv4_LC, IOS_WIFI_LC @"/" IP_ADDR_IPv6_LC, IOS_WIFI_LC @"/" IP_ADDR_IPv4_LC, IOS_CELLULAR_LC @"/" IP_ADDR_IPv6_LC, IOS_CELLULAR_LC @"/" IP_ADDR_IPv4_LC ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
    //    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         if(address) *stop = YES;
     } ];
    return address ? address : @"0.0.0.0";
}

- (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4_LC;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6_LC;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

- (void) sendNSStringLC: (NSString *)sendString{
    //преобразуем в массив и посылаем
    const uint8_t * rawstring = (const uint8_t *)[sendString UTF8String];
    NSInteger bytesWritten = [outputLogCollector write:rawstring maxLength:strlen(rawstring)];
    if (bytesWritten == [sendString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]) {
        
    } else {
        NSLog(@"bytesWritten'%i' == [tekString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]'%i'",bytesWritten,[sendString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
    }
}

#pragma mark - NSStreamDelegate
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)eventCode{
    //    allLogCollector(@"->",vivodProcedur,true,true);
    //#pragma unused(aStream)
    //    assert(aStream == self.networkStream);
    
    switch (eventCode) {
        case NSStreamEventOpenCompleted: {
            if ([theStream isEqual:inputLogCollector]) {
                NSLog(@"Opened inputLogCollector");
            }else if ([theStream isEqual:outputLogCollector]) {
                NSLog(@"Opened outputLogCollector");
            }else{
                NSLog(@"Opened connection");
            }
        } break;
        case NSStreamEventHasBytesAvailable: {
            //            NSLog(@"Получаем данные");
            //            assert(NO);     // should never happen for the output stream
            if ([theStream isEqual:inputLogCollector]) {
                NSMutableData *dataInput = [NSMutableData data];
                bytesReadLC = [NSNumber new];
                NSString *getStringLC = @"";
                
                NSInteger lenLC = kInputBufferSize;
                uint8_t bufLC[lenLC];
                
                while (lenLC==kInputBufferSize) {
                    
                    lenLC = [(NSInputStream *)theStream read:bufLC maxLength:kInputBufferSize];
//                    NSLog(@"%li",(long)lenLC);
                    if(lenLC>0) {
                        
//                        getStringLC = [[NSString alloc] initWithData:dataInput encoding:NSUTF8StringEncoding];
//                        NSString *tstStr=[[NSString alloc] initWithFormat:@"Получили данные (%ld)(%@)",(long)lenLC ,getStringLC];
//                        NSLog(tstStr);
                        
                        if (lenLC<=kInputBufferSize) {
                            
                            [dataInput appendBytes:(const void *)bufLC length:lenLC];
                            
                            if ([dataInput length]>kInputBufferSize*2) {
                                getStringLC = [[NSString alloc] initWithData:dataInput encoding:NSUTF8StringEncoding];
                                if ([self analisData:getStringLC]){
                                    [theStream close];
                                    [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                                    inputLogCollector = nil;
                                }
                                
                                dataInput = [NSMutableData data];
                            }
                            
                        }else{
                            //Значиться начали получать бред
                            NSLog(@"Что-то много данных передали!");
                        }
                    } else {
                        NSLog(@"Нет данных в буффере!");
                    }
                }
                
                @try {
                    if ([dataInput length]>0){
                        getStringLC = [[NSString alloc] initWithData:dataInput encoding:NSUTF8StringEncoding];
//                        NSString *tstStr=[[NSString alloc] initWithFormat:@"Получили данные (%@)", getStringLC];
//                        NSLog(tstStr);
                        //Анализ данных пришедших с сервера
                        if ([self analisData:getStringLC]){
                            [theStream close];
                            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                            inputLogCollector = nil;
                        }
                    }
                    
                }
                @catch (NSException *exception) {
                    
                }
                @finally {
                    
                }
            }
            
        } break;
        case NSStreamEventHasSpaceAvailable: {
            //
            //            NSLog(@"Sending");
            
            if ([theStream isEqual:outputLogCollector]) {
                
                //Представляемся
                NSMutableString* tekString;
                if (firstMess) {
                    @try {
                        tekString = [NSMutableString stringWithFormat:@"EHLO DFS_iOS '%@'(%@)\r\n",[[UIDevice currentDevice] name],[self getIPAddress:YES]];
                    }
                    @catch (NSException *exception) {
                        tekString = [NSMutableString stringWithFormat:@"EHLO DFS_iOS '%@'\r\n",[[UIDevice currentDevice] name]];
                    }
                    
                    //                    tekString = [NSMutableString stringWithFormat:@"EHLO  DFS_iOS '%@'\r\n",[[UIDevice currentDevice] name]];
                    
                    [self sendNSStringLC:tekString];
                    firstMess = NO;
                }
                //Выбираем сообщения из не записанных логов
                if (not_send_messagesLG) {
                    NSLog(@"[messagesLG count]'%i'",[messagesLG count]);
                    if ([messagesLG count]>0) {
                        NSArray *resultsZapxrosNotSave = [NSArray arrayWithArray:messagesLG];
                        [self sendLogString:resultsZapxrosNotSave];
                        //                    [self.messagesLG removeAllObjects];
                        not_send_messagesLG=NO;
                    }
                }
                
                
                //Выбираем сообщения из логов
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSSortDescriptor *timedateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timedate" ascending:NO];//ascending:YES];
                NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:timedateDescriptor, nil];
                assert(timedateDescriptor != nil);
                [fetchRequest setSortDescriptors:sortDescriptors];
                
                NSManagedObjectContext *context = [self managedObjectContext];NSError *error = nil;
                
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"LogDevice" inManagedObjectContext:context];
                assert(entity != nil);
                [fetchRequest setEntity:entity];
                
                NSArray *resultsZaprosAll = [context executeFetchRequest:fetchRequest error:&error];
                //        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"LogDevice"];
                //        NSArray *resultsZaprosAll = [context executeFetchRequest:request error:&error];
                
                NSLog(@"sendLogTo Sending = %i",[resultsZaprosAll count]);
                
                if ([resultsZaprosAll count]>0) {
                    
                    NSDateFormatter *dateFormatter = [NSDateFormatter new];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
                    for (NSManagedObject* tmpObject in resultsZaprosAll) {
                        //Создаём строку логов
                        NSDate *tekTimeDate=[tmpObject valueForKey:@"timedate"];
                        //TODO Проверить с начала наличие символа | в сообщении.
                        NSString* sendString = [[NSString alloc]initWithFormat:@"||%@||%@||%@||%@||%@||%@||%@||%@||\r\n",[tmpObject valueForKey:@"activeObject"],[tmpObject valueForKey:@"appearance"],[tmpObject valueForKey:@"body"],
                                                [tmpObject valueForKey:@"comment"],
                                                [tmpObject valueForKey:@"modul"],
                                                [tmpObject valueForKey:@"numberline"],
                                                [tmpObject valueForKey:@"numberzapusk"],
                                                [dateFormatter stringFromDate:tekTimeDate]];
                        //Преобразуем строку логов в англ
                        NSArray*strRusArray=[[NSArray alloc] initWithObjects: @"А",@"а",@"Б",@"б",@"В",@"в",@"Г",@"г",@"Д",@"д",@"Е",@"е",@"Ё",@"ё",@"Ж",@"ж",@"З",@"з",@"И",@"и",@"Й",@"й",@"К",@"к",@"Л",@"л",@"М",@"м",@"Н",@"н",@"О",@"о",@"П",@"п",@"Р",@"р",@"С",@"с",@"Т",@"т",@"У",@"у",@"Ф",@"ф",@"Х",@"х",@"Ц",@"ц",@"Ч",@"ч",@"Ш",@"ш",@"Щ",@"щ",@"Ъ",@"ъ",@"Ы",@"ы",@"Ь",@"ь",@"Э",@"э",@"Ю",@"ю",@"Я",@"я",@".",@" ",@"|",@"-",@">",@"<",@"(",@")", nil];
                        NSArray*strEngArray=[[NSArray alloc] initWithObjects: @"A",@"a",@"B",@"b",@"V",@"v",@"G",@"g",@"D",@"d",@"E",@"e",@"E",@"e",@"J",@"j",@"Z",@"z",@"I",@"i",@"I",@"i",@"K",@"k",@"L",@"l",@"M",@"m",@"N",@"n",@"O",@"o",@"P",@"p",@"R",@"r",@"S",@"s",@"T",@"t",@"U",@"u",@"F",@"f",@"H",@"h",@"C",@"c",@"CH",@"ch",@"SH",@"sh",@"SHI",@"shi",@"TT",@"tt",@"II",@"ii",@"LL",@"ll",@"IE",@"ie",@"IU",@"iu",@"IA",@"ia",@",",@"_",@"|",@"-",@">",@"<",@"(",@")", nil];
                        tekString = [[NSMutableString alloc] initWithString:sendString];
                        NSInteger tekPos=0;
                        for (NSString*tmpChar in strRusArray) {
                            [tekString replaceOccurrencesOfString:tmpChar withString:strEngArray[tekPos] options:0 range:NSMakeRange(0, [tekString length])];
                            tekPos++;
                        }
                        [self sendNSStringLC:sendString];
                    }
                    
                }
                [self cleanCData:resultsZaprosAll];
                
                //Посылаем команды сервису
                NSArray *tmpCMDs = [NSArray arrayWithArray:cmdLG];
                [cmdLG removeAllObjects];
                
                for (NSString *tekCMD in tmpCMDs) {
                    tekString = [NSMutableString stringWithFormat:@"CMD %@\r\n",tekCMD];
                    [self sendNSStringLC:tekString];
                }
                
                //Посылаем досвидание сервису
                tekString = [NSMutableString stringWithFormat:@"BYBYserver'%@'(%@)\r\n",[[UIDevice currentDevice] name],[self getIPAddress:YES]];
                [self sendNSStringLC:tekString];
                
                [theStream close];
                [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                outputLogCollector = nil;
                
            } else {
                NSLog(@"Пришли данные не понятно от куда.");
            }
        } break;
        case NSStreamEventErrorOccurred: {
            if ([theStream isEqual:outputLogCollector]) {
                NSLog(@"Stream OUT open error");
                [theStream close];
                [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                outputLogCollector = nil;
            } else if ([theStream isEqual:inputLogCollector]) {
                NSLog(@"Stream INP open error");
                [theStream close];
                [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                inputLogCollector = nil;
            }else{
                NSLog(@"Stream open error");
            }
        } break;
        case NSStreamEventEndEncountered: {
            NSLog(@"Завершили данные");
            // ignore
        } break;
        default: {
            NSLog(@"Не понятно что");
            assert(NO);
        } break;
    }
}

- (BOOL) analisData: (NSString *)allData{
    
//    NSLog(@"%@",allData);
    
    NSArray *listItems =[allData componentsSeparatedByString:@"\r\n"];
    NSInteger ij=0;
    
    for (NSString *tekStringLC in listItems) { ij++;
        NSLog(@"%@",tekStringLC);
        
        if ([@"" isEqualToString:tekStringLC]) {
            
        }else if(([tekStringLC length]>9)
                 &&([@"BYBYclient" isEqualToString:[tekStringLC substringToIndex:10]])){
            
            return YES;
            
        }else if(([tekStringLC length]>14)
                 &&([@"CMDB getAllFile" isEqualToString:[tekStringLC substringToIndex:15]])){
            getAllFile = YES;
            otrezFile = @"";
            arrayFiles = [NSMutableArray new];
            
        }else if(([tekStringLC length]>14)
                 &&([@"CMDE getAllFile" isEqualToString:[tekStringLC substringToIndex:15]])){
            getAllFile = NO;
            //TODO Надо-ли сохранять усё за раз
//            NSArray *tmpArray = [NSArray arrayWithArray:arrayFiles];
//            arrayFiles = [NSMutableArray new];
//            [self saveFilesToCD:tmpArray];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            [defaults setValue:@"Получены!" forKey:@"getAllFile"];
            
        }else{
            if (getAllFile) {
                NSString *tmpStringLC=nil;
                
                if ([@"" isEqualToString:otrezFile]) {
                    tmpStringLC = [NSString stringWithFormat:@"%@",tekStringLC];
                }else{
                    tmpStringLC = [NSString stringWithFormat:@"%@%@",otrezFile,tekStringLC];
                    otrezFile=@"";
                }
                
                NSArray *propertysItem = [tmpStringLC componentsSeparatedByString:@"||"];
                tmpStringLC=nil;
                if ([propertysItem count]==13) {
                    otrezFile=@"";
                    [arrayFiles addObject:propertysItem];
                    NSArray *tmpArray = [NSArray arrayWithArray:arrayFiles];
                    arrayFiles = [NSMutableArray new];
                    [self saveFilesToCD:tmpArray];
                }else{
                    otrezFile=tmpStringLC;
                }
            }
        }
    }
    return NO;
}

- (void) sendLogString:(NSArray *)sendArray{
    for (LogDevice_NotSynch* tmpObject in sendArray) {
        //Создаём строку логов
        
        NSString *sendString = [tmpObject toString];
        
        [self sendNSStringLC:sendString];
    }
}

#pragma mark - Core Data stack

//TODO доработать очисткой БД при добавлении таблиц/реквизитов
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
//    NSString *sUrl = [[NSBundle mainBundle] pathForResource:@"LogDevice" ofType:@"momd" inDirectory:@"Class/LogDevice"];
//    NSURL *modelURL = [NSURL URLWithString:sUrl];//[[NSBundle mainBundle] URLForResource:@"LogDevice" withExtension:@"momd" inDirectory:@"Class/LogDevice"];
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LogDevice" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LogDevice.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    if (_daddyManagedObjectContext!=nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc]  initWithConcurrencyType:NSMainQueueConcurrencyType];
        // Добавляем наш приватный контекст отцом, чтобы дочка смогла пушить все изменения
        [_managedObjectContext setParentContext:_daddyManagedObjectContext];
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext:(BOOL)wait {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        
        assert(managedObjectContext!=nil);
        assert(_managedObjectContext!=nil);
        
        if ([managedObjectContext hasChanges]){
            
            if (![managedObjectContext save:&error]) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }else{
                void (^saveDaddyContext) (void) = ^{
                    NSError *error = nil;
                    [_daddyManagedObjectContext save:&error];
                };
                if ([_daddyManagedObjectContext hasChanges]) {
                    if (wait)
                        [_daddyManagedObjectContext performBlockAndWait:saveDaddyContext];
                    else
                        [_daddyManagedObjectContext performBlock:saveDaddyContext];
                }
            }
        }
    }
}

#pragma mark - Сохранить файлы в коре дату
- (void)saveFilesToCD:(NSArray *)arrayFiles{allLogCollector(@"->",vivodProcedur,true,true);
    
    NSNumberFormatter *fNumber = [[NSNumberFormatter alloc] init];
    fNumber.numberStyle = NSNumberFormatterDecimalStyle;
    
    for (NSArray *propertysItem in arrayFiles) {
        
        //Проверим наличие такого файла по id_object
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(id_object = %@)&&(fullpath = %@)", [fNumber numberFromString:((NSString *)propertysItem[1])], propertysItem[10]];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setPredicate:predicate];
        NSManagedObjectContext *context = [ApplicationDelegate managedObjectContext];NSError *error = nil;
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FilesAll" inManagedObjectContext:context];
        assert(entity != nil);
        [fetchRequest setEntity:entity];
        
        NSArray *resultsFoundFile = [context executeFetchRequest:fetchRequest error:&error];
        
        NSString *sName = propertysItem[8];
        
        if ([resultsFoundFile count]==0) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSManagedObjectContext *context = [self getContextForBGTask];
                assert(context != nil);
                
                FilesAll *object = [NSEntityDescription insertNewObjectForEntityForName:@"FilesAll" inManagedObjectContext:context];
                [object setValue:[fNumber numberFromString:((NSString *)propertysItem[1])] forKey:@"id_object"];
                [object setValue:propertysItem[2] forKey:@"crc32"];
                //TODO В предикату добавить фильтр по filesize
                [object setValue:[fNumber numberFromString:((NSString *)propertysItem[3])] forKey:@"filesize"];
                [object setValue:propertysItem[4] forKey:@"crc64"];
                [object setValue:propertysItem[5] forKey:@"mark"];
                [object setValue:propertysItem[6] forKey:@"visa"];
                [object setValue:propertysItem[7] forKey:@"comment"];
                [object setValue:sName forKey:@"name"];
                [object setValue:propertysItem[9] forKey:@"path"];
                [object setValue:propertysItem[10]forKey:@"fullpath"];
                NSString *base64String = propertysItem[11];
                NSData *decodedDataImg = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
                
                assert(decodedDataImg!=nil);
                assert(decodedDataImg!=NULL);
                
                UIImage *retValue = [AppDelegate convertBitmapRGBA8ToUIImage:decodedDataImg withWidth:100 withHeight:50];
                NSData *dataPng = UIImagePNGRepresentation(retValue);
                NSArray *pathsDD = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDir = [pathsDD objectAtIndex:0];
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDir, sName];
                [dataPng writeToFile:filePath atomically:NO];
                
//                NSInteger width = 100;
//                NSInteger height = 50;
//                
//                CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//                size_t numberOfComponents = CGColorSpaceGetNumberOfComponents(colorSpace) + 1;
//                size_t bitsPerComponent = 8;
//                size_t bytesPerPixel = (bitsPerComponent * numberOfComponents) / 8;
//                size_t bytesPerRow = bytesPerPixel * width;
//                
//                uint8_t *rawData = (uint8_t*)calloc(width * height * numberOfComponents, sizeof(uint8_t));
//                CGContextRef contextImg = CGBitmapContextCreate(rawData,
//                                                             width,
//                                                             height,
//                                                             bitsPerComponent,
//                                                             bytesPerRow,
//                                                             colorSpace,
//                                                             kCGImageAlphaPremultipliedLast);
//                CGColorSpaceRelease(colorSpace);
//                
//                CGContextDrawImage(context, CGRectMake(0, 0, width, height), [anImage CGImage]);
//                
//                CGContextRelease(context);
//                
//                //Тестовое сохранения данных в файл для просмотра полученных миниатюр
//                NSArray *pathsDD = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsDir = [pathsDD objectAtIndex:0];
//                NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDir, sName];
//                NSString *filePathTest = [NSString stringWithFormat:@"%@/%@", documentsDir, sName];
//                
//                [decodedDataImg writeToFile:filePathTest atomically:NO];
//                
//                UIImage *retValue = [UIImage imageWithData:decodedDataImg];
//                NSData *dataJpeg = UIImagePNGRepresentation(retValue);//JPEGRepresentation(retValue, 1.0);
//                
//                [dataJpeg writeToFile:filePath atomically:NO];
                
                [object setValue:dataPng   forKey:@"sico"];
                
//                [self saveContext:YES];
                
                if ([context hasChanges]){
                    NSError *error = nil;
                    if (![context save:&error]) {
                        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                        abort();
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self saveContext:YES];
                    NSLog(@"save'%@'(%lul)",sName, (unsigned long)[dataPng length]);
                });
            });
        }else{
            
            NSString *base64String = propertysItem[11];
            NSData *decodedDataImg = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
            
            //Тестовое сохранения данных в файл для просмотра полученных миниатюр
            NSArray *pathsDD = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDir = [pathsDD objectAtIndex:0];
            NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDir, sName];
            NSString *filePathTest = [NSString stringWithFormat:@"%@/%@", documentsDir, sName];
            
            [decodedDataImg writeToFile:filePathTest atomically:NO];
            
            UIImage *retValue = [UIImage imageWithData:decodedDataImg];
            NSData *dataJpeg = UIImageJPEGRepresentation(retValue, 1.0);
            [dataJpeg writeToFile:filePath atomically:NO];
            
            FilesAll *object = resultsFoundFile[0];
            object.sico = decodedDataImg;
        }
        
    }
    
}

+ (UIImage *) convertBitmapRGBA8ToUIImage:(NSData *) dataPic
                                withWidth:(int) width
                               withHeight:(int) height {
    
    NSUInteger size = [dataPic length] / sizeof(unsigned char);
    unsigned char* buffer = (unsigned char*) [dataPic bytes];
    
    NSData* dataTest = [NSData dataWithBytes:(const void *)dataPic length:sizeof(unsigned char)*size];
    
    char* rgba = (char*)malloc(width*height*4);
    for(int i=0; i < width*height; ++i) {
        //Для входящего буфера rgb
//        rgba[4*i] = buffer[3*i];
//        rgba[4*i+1] = buffer[3*i+1];
//        rgba[4*i+2] = buffer[3*i+2];
//        rgba[4*i+3] = 255;
        //Для входящего буфера rgba
//        rgba[4*i] = buffer[4*i];
//        rgba[4*i+1] = buffer[4*i+1];
//        rgba[4*i+2] = buffer[4*i+2];
//        rgba[4*i+3] = buffer[4*i+3];
        //Для входящего буфера bgr (java == TYPE_3BYTE_BGR)
        rgba[4*i] = buffer[3*i+2];
        rgba[4*i+1] = buffer[3*i+1];
        rgba[4*i+2] = buffer[3*i];
        rgba[4*i+3] = 255;
    }
    
    size_t bufferLength = width * height * 4;
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, rgba, bufferLength, NULL);
    size_t bitsPerComponent = 8;
    size_t bitsPerPixel = 32;
    size_t bytesPerRow = 4 * width;
    
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    if(colorSpaceRef == NULL) {
        NSLog(@"Error allocating color space");
        CGDataProviderRelease(provider);
        return nil;
    }
    
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaPremultipliedLast;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    CGImageRef iref = CGImageCreate(width,
                                    height,
                                    bitsPerComponent,
                                    bitsPerPixel,
                                    bytesPerRow,
                                    colorSpaceRef,
                                    bitmapInfo,
                                    provider,   // data provider
                                    NULL,       // decode
                                    YES,            // should interpolate
                                    renderingIntent);
    
    uint32_t* pixels = (uint32_t*)malloc(bufferLength);
    
    if(pixels == NULL) {
        NSLog(@"Error: Memory not allocated for bitmap");
        CGDataProviderRelease(provider);
        CGColorSpaceRelease(colorSpaceRef);
        CGImageRelease(iref);
        return nil;
    }
    
    CGContextRef context = CGBitmapContextCreate(pixels,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpaceRef,
                                                 bitmapInfo);
    
    if(context == NULL) {
        NSLog(@"Error context not created");
        free(pixels);
    }
    
    UIImage *image = nil;
    if(context) {
        
        CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, width, height), iref);
        
        CGImageRef imageRef = CGBitmapContextCreateImage(context);
        
        // Support both iPad 3.2 and iPhone 4 Retina displays with the correct scale
        if([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
            float scale = [[UIScreen mainScreen] scale];
            image = [UIImage imageWithCGImage:imageRef scale:scale orientation:UIImageOrientationUp];
        } else {
            image = [UIImage imageWithCGImage:imageRef];
        }
        
        CGImageRelease(imageRef);
        CGContextRelease(context);
    }
    
    CGColorSpaceRelease(colorSpaceRef);
    CGImageRelease(iref);
    CGDataProviderRelease(provider);
    
    if(pixels) {
        free(pixels);
    }   
    return image;
}

@end
//Лично я просто поставил TorBrowser, но запускаю при этом только тор из его состава.
//Заходим в "Программы" и находим там TorBrowser. В контекстном меню выбираем "Показать содержимое пакета". Далее переходим в папку "TorBrowser/Tor" (полный путь у меня получается "/Applications/TorBrowser.app/TorBrowser/Tor") . Там, помимо прочего есть 2 файла: "tor" и "tor.real". Вот "tor.real" это и есть настоящий тор, а файл "tor" это просто батник который его запускает. Ну я обычно пользуюсь этим самым батником. Вполне удобно. Запускается терминал, а в нем тор. Далее остается только настроить прокси в вашем браузере. (Тип прокси SOCKS v5, host - 127.0.0.1, port - 9050, удаленный DNS - true). В Firefox для удобства использую расширение Proxy Switcher. Для хрома уж что-нибудь свое придумаете, там наверняка есть аналог.
//Единственный тонкий момент, для того чтобы завершить работу тора недостаточно просто закрыть окно терминала. Надо сначала в терминале нажать "ctrl+C", иначе процесс останется висеть. Впрочем его потом можно будет убить через мониторинг системы, называется "tor.real". С другой стороны его и завершать-то необязательно. Пускай себе висит, а мы можем переключаться в браузере, выбирая использовать прокси или нет.
//З.Ы. Пока вроде все работает, и в браузере и в торренте.
//З.З.Ы. Есть еще более простой вариант, который я использовал, когда тестировал свою идею. Просто запускаем TorBrowser и сворачиваем его. Далее настраиваем тор в качестве прокси в любом своем браузере и торрент клиенте. Единственное отличие в настройках - он почему-то использует порт 9150, а не 9050. В любом случае, актуальные настройки всегда можно посмотреть в сетевых настройках самого TorBrowser. Находятся они в том же месте, как и у стандартного Firefox. Все будет работать, до тех пор, пока запущен TorBrowser.