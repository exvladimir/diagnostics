//
//  FilesAll+CoreDataProperties.h
//  diagnostics
//
//  Created by Рашкин Владимир Валерьевич on 26/05/16.
//  Copyright © 2016 vrashkin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FilesAll.h"

NS_ASSUME_NONNULL_BEGIN

@interface FilesAll (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSString *crc32;
@property (nullable, nonatomic, retain) NSString *crc64;
@property (nullable, nonatomic, retain) NSData *databyte;
@property (nullable, nonatomic, retain) NSNumber *filesize;
@property (nullable, nonatomic, retain) NSString *fullpath;
@property (nullable, nonatomic, retain) NSNumber *id_object;
@property (nullable, nonatomic, retain) NSString *mark;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSData *sico;
@property (nullable, nonatomic, retain) NSString *visa;

@end

NS_ASSUME_NONNULL_END
