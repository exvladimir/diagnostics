//
//  SecondViewController.m
//  diagnostics
//
//  Created by vrashkin on 22/02/15.
//  Copyright (c) 2015 vrashkin. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) onPress_getScreen:(id)sender{
    [self showAlert:@"onPress_getScreen" atTitle:@"Test"];
}

- (void) showAlert: (NSString *)vibMessage atTitle:(NSString *)vibTitle {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:vibTitle
                                           message:vibMessage
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
    
    [alertView show];
}

@end
