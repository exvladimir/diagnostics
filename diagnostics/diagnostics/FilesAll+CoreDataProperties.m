//
//  FilesAll+CoreDataProperties.m
//  diagnostics
//
//  Created by Рашкин Владимир Валерьевич on 26/05/16.
//  Copyright © 2016 vrashkin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FilesAll+CoreDataProperties.h"

@implementation FilesAll (CoreDataProperties)

@dynamic comment;
@dynamic crc32;
@dynamic crc64;
@dynamic databyte;
@dynamic filesize;
@dynamic fullpath;
@dynamic id_object;
@dynamic mark;
@dynamic name;
@dynamic path;
@dynamic sico;
@dynamic visa;

@end
