//
//  FilesViewController.m
//  diagnostics
//
//  Created by Рашкин Владимир Валерьевич on 25/05/16.
//  Copyright © 2016 vrashkin. All rights reserved.
//

#import "FilesViewController.h"
#import "AppDelegate.h"

@interface FilesViewController ()

@end

@implementation FilesViewController
@synthesize o_FilesTable;

- (void)viewDidLoad {allLogCollector(@"->",vivodProcedur,true,true);
    [super viewDidLoad];
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//    [self.tableView beginUpdates];
//    [self.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSFetchedResultsController *)fetchedResultsController {allLogCollector(@"->",vivodProcedur,true,true);
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSManagedObjectContext *tmpContext = [ApplicationDelegate managedObjectContext];
    
    assert(tmpContext!=nil);
    
    //TODO Срочно разобраться почему нижекомментируемый код не работает а расскомментированный код работает.
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription
//                                   entityForName:@"FilesAll" inManagedObjectContext:tmpContext];
//    [fetchRequest setEntity:entity];
//    
//    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
//                              initWithKey:@"name" ascending:NO];
//    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
//    
//    NSError *tekError = nil;
//    @try {
//        NSArray *resultsZaprosAll = [tmpContext executeFetchRequest:fetchRequest error:&tekError];
//        NSLog(@"%lu",[resultsZaprosAll count]);
//    }
//    @catch (NSException *exception) {
//        NSString *tstStr=[[NSString alloc] initWithFormat:@"(%@)(%@)",exception.reason, tekError];
//        allLogCollectorNSLog(tstStr, 5, true,true,true);
//    }
//    
////    [fetchRequest setFetchBatchSize:20];
//    
//    NSFetchedResultsController *theFetchedResultsController =
//    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
//                                        managedObjectContext:tmpContext sectionNameKeyPath:nil
//                                                   cacheName:@"Root"];
//    self.fetchedResultsController = theFetchedResultsController;
//    _fetchedResultsController.delegate = self;
//    return _fetchedResultsController;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"FilesAll"];
    [fetch setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    
    NSManagedObjectContext *moc = tmpContext;
    NSFetchedResultsController *frc = nil;
    
    frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetch managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
    frc.delegate = self;
    self.fetchedResultsController = frc;
    
    NSError *error = nil;
    if (![frc performFetch:&error]) {
        NSLog(@"Error: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    
    return frc;
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {allLogCollector(@"->",vivodProcedur,true,true);
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.o_FilesTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {allLogCollector(@"->",vivodProcedur,true,true);
    
    UITableView *tableView = self.o_FilesTable;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {allLogCollector(@"->",vivodProcedur,true,true);
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.o_FilesTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.o_FilesTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {allLogCollector(@"->",vivodProcedur,true,true);
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.o_FilesTable endUpdates];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {allLogCollector(@"->",vivodProcedur,true,true);
//#warning Incomplete implementation, return the number of sections
//    return 1;
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {allLogCollector(@"->",vivodProcedur,true,true);
    
//    id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    id <NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    
    NSLog(@"%lu",[sectionInfo numberOfObjects]);
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilesCell"];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)viewDidUnload {allLogCollector(@"->",vivodProcedur,true,true);
    
    self.fetchedResultsController = nil;
    
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    FilesAll *oFiles = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:331];
    nameLabel.text = [oFiles name];
    
    UILabel *gameLabel = (UILabel *)[cell viewWithTag:332];
    gameLabel.text = [oFiles path];
    
    UIImageView *ratingImageView = (UIImageView *)[cell viewWithTag:333];
    ratingImageView.image = [self imageForSico:[oFiles sico]];
    
    //TODO Проверка автоматическая :)
//    if ([@"IMG_0288.JPG" isEqualToString:[oFiles name]]) {
//        ratingImageView.image = [self imageForSico:[oFiles sico]];
//    }
    
    
//    cell.textLabel.text = [oFiles name];
//    cell.detailTextLabel.text = [oFiles path];
    
}

- (UIImage *)imageForSico:(NSData *)vibData
{
    UIImage *retValue = [UIImage imageWithData:vibData];
    
    return retValue;
}

- (UIImage *)imageForVisa:(NSString *)vibVisa
{
    UIImage *retValue = nil;
    if ([@"0" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"miniPic"];
    }else if ([@"*" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"1StarSmall"];
    }else if ([@"**" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"2StarSmall"];
    }else if ([@"***" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"3StarSmall"];
    }else if ([@"****" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"4StarSmall"];
    }else if ([@"****" isEqualToString:vibVisa]) {
        retValue=[UIImage imageNamed:@"5StarSmall"];
    }
    return retValue;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
