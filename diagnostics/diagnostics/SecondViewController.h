//
//  SecondViewController.h
//  diagnostics
//
//  Created by vrashkin on 22/02/15.
//  Copyright (c) 2015 vrashkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property(retain) IBOutlet UIButton *getScreen;

- (IBAction) onPress_getScreen:(id)sender;

@end

