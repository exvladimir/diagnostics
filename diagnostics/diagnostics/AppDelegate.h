//
//  AppDelegate.h
//  diagnostics
//
//  Created by vrashkin on 22/02/15.
//  Copyright (c) 2015 vrashkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>

#include <dlfcn.h>
#include <asl.h>

#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

#import <CoreData/CoreData.h>
#import "FilesViewController.h"
#import "LogDevice.h"
#import "LogDevice_NotSynch.h"

@class FilesViewController;

#define vivodProcedur YES
//Облегченная, используется 1 раз в процедуру (содает временное значение)
#define allLogCollector(tekStringInfo,VivoditMess,SohraniatMess,PeredavatMess)\
NSArray *tekArrayLogCollector = [[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "];\
if (VivoditMess)   { NSLog(@"%@ %@ (%@::%d)",tekStringInfo,tekArrayLogCollector[0],tekArrayLogCollector[1],__LINE__);};\
if (SohraniatMess) { [((AppDelegate *) [[UIApplication sharedApplication] delegate]) SohraniatMessInCD:tekStringInfo appearance:10 modul:tekArrayLogCollector[0] procedure:tekArrayLogCollector[1] numberLine:__LINE__];};

//Используется несколько раз за процедуру
#define allLogCollectorNSLog(tekStringInfo,tekAppearance,VivoditMess,SohraniatMess,PeredavatMess)\
if (VivoditMess)   { NSLog(@"%@ %@ (%@::%d)",tekStringInfo,[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][0],[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][1],__LINE__);};\
if (SohraniatMess) { [((AppDelegate *) [[UIApplication sharedApplication] delegate]) SohraniatMessInCD:tekStringInfo appearance:tekAppearance modul:[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][0] procedure:[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][1] numberLine:__LINE__];};

#define allLogCollectorNSLogWithOrder(tekStringInfo,tekAppearance,VivoditMess,SohraniatMess,PeredavatMess,tekCode_order, tekAction_order, tekCounteragent_order, tekGuid_order)\
if (VivoditMess)   { NSLog(@"%@ %@ (%@::%d)",tekStringInfo,[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][0],[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][1],__LINE__);};\
if (SohraniatMess) { [((AppDelegate *) [[UIApplication sharedApplication] delegate]) SohraniatMessInCDWithOrder:tekStringInfo appearance:tekAppearance modul:[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][0] procedure:[[[[[[NSString alloc] initWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""] stringByReplacingOccurrencesOfString:@"-" withString:@""] componentsSeparatedByString:@" "][1] numberLine:__LINE__ code_order:tekCode_order action_order:tekAction_order counteragent_order:tekCounteragent_order guid_order:tekGuid_order];};

#define ApplicationDelegate                 ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define IOS_CELLULAR_LC    @"pdp_ip0"
#define IOS_WIFI_LC        @"en0"
#define IOS_VPN_LC         @"utun0"
#define IP_ADDR_IPv4_LC    @"ipv4"
#define IP_ADDR_IPv6_LC    @"ipv6"
#pragma mark - Размер буфера отдачи данных по LogCollector
enum {
    kSendBufferSize = 65536,//32768
    kInputBufferSize = 2048//3072//32768
};

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSStreamDelegate>{
    
    BOOL getAllFile;
    NSString *otrezFile;
    NSMutableArray *arrayFiles;
    
    NSInteger   numberzapusk;
    
    FilesViewController *filesViewController;
    UITabBarController *tabBarController;
    UINavigationController *navigationController;
    
@private
    NSOutputStream *  outputLogCollector;
    NSInputStream *   inputLogCollector;
    NSMutableArray * messagesLG;
    NSMutableArray * cmdLG;
    BOOL not_send_messagesLG;
    NSArray * messagesOutputLG;
    NSNumber *bytesReadLC;
    BOOL initMOC;
    BOOL firstMess;
    NSManagedObjectContext *_daddyManagedObjectContext;
    
    __weak AppDelegate * weakSelf;
    NSDate * dateTimerBegin;
}
@property() UIBackgroundTaskIdentifier backgroundTaskIdentifier;

@property (nonatomic, strong, readwrite) NSOutputStream *  outputLogCollector;
@property (nonatomic, strong, readwrite) NSInputStream *   inputLogCollector;
@property (nonatomic, strong, readwrite) NSMutableArray *   messagesLG;
@property (nonatomic, strong, readonly) NSArray *   messagesOutputLG;
@property (strong) NSTimer *syncTimerLG;

@property (strong, nonatomic) UIWindow *window;

//CoreData
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSManagedObjectContext *)getContextForBGTask;
- (void) SohraniatMessInCD:(NSString*)sendingMessage appearance:(NSInteger)tekAppearance modul:(NSString*)tekModul procedure:(NSString*)tekProcedure numberLine:(NSInteger)tekNumberLine;
- (void) SohraniatMessInCDWithOrder:(NSString*)sendingMessage appearance:(NSInteger)tekAppearance modul:(NSString*)tekModul procedure:(NSString*)tekProcedure numberLine:(NSInteger)tekNumberLine code_order:(NSString*)tekCode_order action_order:(NSString*)tekAction_order counteragent_order:(NSString*)tekCounteragent_order guid_order:(NSString*)tekGuid_order;
- (void)saveContext:(BOOL)wait;
- (NSURL *)applicationDocumentsDirectory;

- (void)runAfter_applicationDidFinishLaunching;


@end

