//
//  LogDevice.m
//  DFS Mobile
//
//  Created by Рашкин Владимир Валерьевич on 23/05/16.
//  Copyright © 2016 Elena Pankratova. All rights reserved.
//

#import "LogDevice.h"

@implementation LogDevice
@synthesize activeObject_NotSynch=activeObject_NotSynch, appearance_NotSynch=appearance_NotSynch, body_NotSynch=body_NotSynch, comment_NotSynch=comment_NotSynch,modul_NotSynch=modul_NotSynch,numberline_NotSynch=numberline_NotSynch,numberzapusk_NotSynch=numberzapusk_NotSynch,timedate_NotSynch=timedate_NotSynch;

// Insert code here to add functionality to your managed object subclass
- (NSString *)toString {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:ss"];
    
    NSDate *tekTimeDate=[self valueForKey:@"timedate"];
    NSString* returnString = [NSString new];
    
    if (activeObject_NotSynch==nil) {
        //TODO Проверить с начала наличие символа | в сообщении.
//        returnString = [[NSString alloc]initWithFormat:@"||%@||%@||%@||%@||%@||%@||%@||%@||\r\n",
//                            [self activeObject],
//                            [self appearance],
//                            [self body],
//                            [self comment],
//                            [self modul],
//                            [self numberline],
//                            [self numberzapusk],
//                            [dateFormatter stringFromDate:tekTimeDate]];
    } else {
        //TODO Проверить с начала наличие символа | в сообщении.
        returnString = [[NSString alloc]initWithFormat:@"||%@||%@||%@||%@||%@||%@||%@||%@||\r\n",
                        [self activeObject_NotSynch],
                        [self appearance_NotSynch],
                        [self body_NotSynch],
                        [self comment_NotSynch],
                        [self modul_NotSynch],
                        [self numberline_NotSynch],
                        [self numberzapusk_NotSynch],
                        [dateFormatter stringFromDate:tekTimeDate]];
    }
    
    
    //Преобразуем строку логов в англ
    NSArray*strRusArray=[[NSArray alloc] initWithObjects: @"А",@"а",@"Б",@"б",@"В",@"в",@"Г",@"г",@"Д",@"д",@"Е",@"е",@"Ё",@"ё",@"Ж",@"ж",@"З",@"з",@"И",@"и",@"Й",@"й",@"К",@"к",@"Л",@"л",@"М",@"м",@"Н",@"н",@"О",@"о",@"П",@"п",@"Р",@"р",@"С",@"с",@"Т",@"т",@"У",@"у",@"Ф",@"ф",@"Х",@"х",@"Ц",@"ц",@"Ч",@"ч",@"Ш",@"ш",@"Щ",@"щ",@"Ъ",@"ъ",@"Ы",@"ы",@"Ь",@"ь",@"Э",@"э",@"Ю",@"ю",@"Я",@"я",@".",@" ",@"|",@"-",@">",@"<",@"(",@")", nil];
    NSArray*strEngArray=[[NSArray alloc] initWithObjects: @"A",@"a",@"B",@"b",@"V",@"v",@"G",@"g",@"D",@"d",@"E",@"e",@"E",@"e",@"J",@"j",@"Z",@"z",@"I",@"i",@"I",@"i",@"K",@"k",@"L",@"l",@"M",@"m",@"N",@"n",@"O",@"o",@"P",@"p",@"R",@"r",@"S",@"s",@"T",@"t",@"U",@"u",@"F",@"f",@"H",@"h",@"C",@"c",@"CH",@"ch",@"SH",@"sh",@"SHI",@"shi",@"TT",@"tt",@"II",@"ii",@"LL",@"ll",@"IE",@"ie",@"IU",@"iu",@"IA",@"ia",@",",@"_",@"|",@"-",@">",@"<",@"(",@")", nil];
    
    NSMutableString*  tekString = [[NSMutableString alloc] initWithString:returnString];
    
    NSInteger tekPos=0;
    for (NSString*tmpChar in strRusArray) {
        
        [tekString replaceOccurrencesOfString:tmpChar withString:strEngArray[tekPos] options:0 range:NSMakeRange(0, [tekString length])];
        tekPos++;
    }
    
    return returnString;
}
@end
