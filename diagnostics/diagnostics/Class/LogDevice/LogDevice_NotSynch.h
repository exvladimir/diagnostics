//
//  LogDevice.h
//  DFS Mobile
//
//  Created by Рашкин Владимир Валерьевич on 23/05/16.
//  Copyright © 2016 Elena Pankratova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface LogDevice_NotSynch : NSObject{
    NSString * activeObject;
    NSNumber * appearance;
    NSString * body;
    NSString * comment;
    NSString * modul;
    NSNumber * numberline;
    NSNumber * numberzapusk;
    NSDate * timedate;
}
@property (nonatomic, strong, readwrite) NSString *activeObject;
@property (nonatomic, strong, readwrite) NSNumber *appearance;
@property (nonatomic, strong, readwrite) NSString *body;
@property (nonatomic, strong, readwrite) NSString *comment;
@property (nonatomic, strong, readwrite) NSString *modul;
@property (nonatomic, strong, readwrite) NSNumber *numberline;
@property (nonatomic, strong, readwrite) NSNumber *numberzapusk;
@property (nonatomic, strong, readwrite) NSDate *timedate;

- (NSString *)toString;

@end
