//
//  LogDevice+CoreDataProperties.m
//  DFS Mobile
//
//  Created by Рашкин Владимир Валерьевич on 23/05/16.
//  Copyright © 2016 Elena Pankratova. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LogDevice+CoreDataProperties.h"

@implementation LogDevice (CoreDataProperties)

@dynamic activeObject;
@dynamic appearance;
@dynamic body;
@dynamic comment;
@dynamic modul;
@dynamic numberline;
@dynamic numberzapusk;
@dynamic timedate;

@end
