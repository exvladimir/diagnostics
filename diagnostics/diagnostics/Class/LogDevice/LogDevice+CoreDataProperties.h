//
//  LogDevice+CoreDataProperties.h
//  DFS Mobile
//
//  Created by Рашкин Владимир Валерьевич on 23/05/16.
//  Copyright © 2016 Elena Pankratova. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LogDevice.h"

NS_ASSUME_NONNULL_BEGIN

@interface LogDevice (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *activeObject;
@property (nullable, nonatomic, retain) NSNumber *appearance;
@property (nullable, nonatomic, retain) NSString *body;
@property (nullable, nonatomic, retain) NSString *comment;
@property (nullable, nonatomic, retain) NSString *modul;
@property (nullable, nonatomic, retain) NSNumber *numberline;
@property (nullable, nonatomic, retain) NSNumber *numberzapusk;
@property (nullable, nonatomic, retain) NSDate *timedate;

@end

NS_ASSUME_NONNULL_END
