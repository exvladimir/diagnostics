//
//  LogDevice.h
//  DFS Mobile
//
//  Created by Рашкин Владимир Валерьевич on 23/05/16.
//  Copyright © 2016 Elena Pankratova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LogDevice : NSManagedObject{
    NSString * activeObject_NotSynch;
    NSNumber * appearance_NotSynch;
    NSString * body_NotSynch;
    NSString * comment_NotSynch;
    NSString * modul_NotSynch;
    NSNumber * numberline_NotSynch;
    NSNumber * numberzapusk_NotSynch;
    NSDate * timedate_NotSynch;
}
@property (nonatomic, strong, readwrite) NSString *activeObject_NotSynch;
@property (nonatomic, strong, readwrite) NSNumber *appearance_NotSynch;
@property (nonatomic, strong, readwrite) NSString *body_NotSynch;
@property (nonatomic, strong, readwrite) NSString *comment_NotSynch;
@property (nonatomic, strong, readwrite) NSString *modul_NotSynch;
@property (nonatomic, strong, readwrite) NSNumber *numberline_NotSynch;
@property (nonatomic, strong, readwrite) NSNumber *numberzapusk_NotSynch;
@property (nonatomic, strong, readwrite) NSDate *timedate_NotSynch;

- (NSString *)toString;

@end

NS_ASSUME_NONNULL_END

#import "LogDevice+CoreDataProperties.h"
