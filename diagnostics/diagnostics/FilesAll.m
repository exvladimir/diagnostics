//
//  FilesAll.m
//  diagnostics
//
//  Created by Рашкин Владимир Валерьевич on 26/05/16.
//  Copyright © 2016 vrashkin. All rights reserved.
//

#import "FilesAll.h"

@implementation FilesAll

// Insert code here to add functionality to your managed object subclass
-(NSString *)toString{
    return [NSString stringWithFormat:@"crc32'%@',filesize'%@',name'%@',id_object'%@',crc64'%@',comment'%@',databyte'%@',fullpath'%@',mark'%@',path'%@',sico'%@',visa'%@'",self.crc32,self.filesize,self.name,self.id_object,self.crc64,self.comment,self.databyte,self.fullpath,self.mark,self.path,self.sico,self.visa];
}
@end
